{ config, pkgs, ... }@args:
let

  tomlFile = file: builtins.fromTOML (builtins.readFile file);

  mypkgs = import ./pkgs { inherit pkgs config; };

  hostname = "u";
  user = "u";

  unfree = pkg: pkg.overrideAttrs (attrs: attrs // { meta.license = null; });

in {
  imports = [ ./hardware-configuration.nix ];
  ## Boot
  boot = {
    cleanTmpDir = true;
    plymouth.enable = true;
  };
  ## Firmware
  boot.extraModulePackages = with config.boot.kernelPackages; [ rtl88x2bu ];
  ## Fonts
  fonts = {
    enableFontDir = true;
    fonts = [ (pkgs.nerdfonts.override (a: { fonts = [ "Agave" ]; })) ];
    fontconfig.defaultFonts = {
      monospace = [ "agave Nerd Font Mono" ];
      sansSerif = [ "agave Nerd Font Mono" ];
      serif = [ "agave Nerd Font Mono" ];
    };
  };
  ## Time
  time = {
    timeZone = "Europe/Zaporozhye";
    hardwareClockInLocalTime = true;
  };
  ## Optimize /nix/store
  nix.autoOptimiseStore = true;
  nix.maxJobs = "auto";
  ## Add caches
  nix = {
    binaryCaches = [
      "https://hydra.iohk.io"
      # "https://iohk.cachix.org"
      # "https://all-hies.cachix.org"
    ];
    binaryCachePublicKeys = [
      # "hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ="
      # "iohk.cachix.org-1:DpRUyj7h7V830dp/i6Nti+NEO2/nhblbov/8MW7Rqoo="
      # "all-hies.cachix.org-1:JjrzAOEUsD9ZMt8fdFbzo3jNAyEWlPAwdVuHw4RD43k="
    ];
  };
  # Virtualbox
  virtualisation = {
    libvirtd = {
      enable = true;
      onBoot = "ignore";
    };
    virtualbox.host.enable = true;
    virtualbox.host.enableExtensionPack = true;
    anbox.enable = true;
  };
  # Unfree packages
  nixpkgs.config.allowUnfree = true;
  # Home Manager
  home-manager = {
    useUserPackages = true;
    useGlobalPkgs = true;
    users.${user} = import ./home.nix args;
  };
  ## ADB
  programs.adb.enable = true;
  ## CCache
  programs.ccache = {
    enable = true;
    packageNames = [ "fltk" ];
  };
  ## Networking
  networking = {
    hostName = hostname;
    usePredictableInterfaceNames = false;
    networkmanager.enable = true;
    firewall = {
      enable = true;
      allowPing = false;
      allowedTCPPorts = [
        139 # Samba
        445 # Samba
        8200 # MiniDLNA
      ];
      allowedUDPPorts = [
        5353 # Avahi
        137 # Samba
        138 # Samba
        1900 # DLNA
      ];
    };
  };
  ## Services
  services = {
    gvfs.enable = true;
    nscd.enable = true;

    minidlna = {
      enable = true;
      mediaDirs = [ "/home/${user}/Public" ];
      announceInterval = 60;
    };

    earlyoom = {
      enable = true;
      useKernelOOMKiller = true;
    };

    samba = {
      enable = true;
      extraConfig = "map to guest = Bad User";
      shares.public = {
        path = "/home/u/Public";
        "force user" = user;
        "force group" = "users";
        "read only" = false;
        "guest ok" = true;
        "guest only" = true;
      };
    };

    avahi = {
      enable = true;
      nssmdns = true;
      publish = {
        enable = true;
        userServices = true;
        workstation = true;
      };
    };

    ipfs = {
      enable = false;
      extraConfig = {
        API.HTTPHeaders = {
          Access-Control-Allow-Origin = [
            "https://webui.ipfs.io"
            "http://localhost:3000"
            "http://localhost:8000"
            "http://127.0.0.1:5001"
          ];
          Access-Control-Allow-Methods = [ "PUT" "POST" ];
        };
      };
    };

    logrotate = {
      enable = true;
      extraConfig = ''
        /var/log/* {
          weekly
          rotate 3
          size 10M
          compress
          delaycompress
        }
      '';
    };
  };

  ## Xorg
  services.xserver = {
    enable = true;
    layout = "us,ru";
    xkbOptions = "grp:caps_toggle,compose:ralt";

    displayManager.defaultSession = "mate";

    desktopManager.xterm.enable = false;
    desktopManager.mate.enable = true;

    displayManager = {
      autoLogin = {
        enable = true;
        inherit user;
      };
      sddm.enable = true;
    };
  };

  # https://github.com/NixOS/nixpkgs/issues/97795
  systemd.services.display-manager = {
    wants = [ "multi-user.target" "network-online.target" ];
    after = [ "multi-user.target" "network-online.target" ];
  };

  environment.systemPackages = with pkgs; [
    man-pages
  ];

  ## Users
  users.mutableUsers = false;
  users.users.${user} = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" ];
    hashedPassword =
      "$5$KQF8Vy/pmtUIaru5$UddNjVTx.rEmaB2aIaqtCli0GIp1xGoIpprJfI2r29D";
    packages = with pkgs; [
      codeblocks
      firefox
      dillo
      libreoffice
      tdesktop
      (unfree discord)
      (unfree teams)
      virt-manager
      vlc
      xorg.xkill
      xsel
    ];
    shell = pkgs.bashInteractive;
    openssh.authorizedKeys.keyFiles = [ ./secret/ssh/main.pub ];
  };

  # This value determines the NixOS release which your system is to be
  # compatible, in order to avoid breaking some software such as the database
  # servers. You should change this only after NixOS release notes say you
  # should.

  system.stateVersion = "20.09"; # Did you read the comment?
}
