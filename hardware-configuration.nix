{ ... }: {

  boot.initrd.availableKernelModules =
    [ "xhci_pci" "ahci" "usbhid" "usb_storage" "sd_mod" ];
  boot.initrd.kernelModules = [ "dm-snapshot" ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.supportedFilesystems = [ "exfat" ];

  hardware = {
    enableRedistributableFirmware = true;
    pulseaudio.enable = true;
  };

  sound.enable = true;

  boot.loader = {
    grub = {
      enable = true;
      version = 2;
      device = "nodev";
      useOSProber = true;
      efiSupport = true;
      efiInstallAsRemovable = true;
    };
    efi = {
      canTouchEfiVariables = false;
      efiSysMountPoint = "/boot";
    };
  };

  boot.initrd.luks.devices."root".device =
    "/dev/disk/by-uuid/31762005-5a75-4f51-a72d-2a842304d2ea";

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/a91b5315-2d23-42c3-9fca-ce67bc7b6e84";
    fsType = "ext4";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/C141-B189";
    fsType = "vfat";
  };

  swapDevices = [{ device = "/swap"; }];
}
