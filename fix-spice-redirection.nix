{ pkgs, ... }: {
  environment.systemPackages = [ pkgs.spice-gtk ];
  security.wrappers.spice-client-glib-usb-acl-helper.source =
    "${pkgs.spice-gtk}/bin/spice-client-glib-usb-acl-helper";
}
