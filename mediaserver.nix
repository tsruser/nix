pkgs: {
  autoStart = true;
  ephemeral = true;

  privateNetwork = true;
  hostAddress = "192.168.2.1";
  localAddress = "192.168.2.2";

  forwardPorts = [
    # Jellyfin
    {
      containerPort = 8096;
      hostPort = 8090;
    }
    # Jackett
    {
      containerPort = 9117;
      hostPort = 8091;
    }
    # Radarr
    {
      containerPort = 7878;
      hostPort = 8092;
    }
  ];

  bindMounts."/state" = {
    hostPath = "/var/lib/mediaserver";
    isReadOnly = false;
  };

  config = {
    system.stateVersion = "20.03";
    networking.firewall.enable = false;
    environment.systemPackages = with pkgs; [ traceroute nmap ];

    ## Jellyfin
    systemd.services.jellyfin = {
      enable = true;
      description = "Jellyfin media server.";
      after = [ "network.target" ];
      path = [ pkgs.jellyfin ];
      script = ''
        mkdir -p /state/jellyfin/{data,config,log}
        jellyfin                    \
          -d /state/jellyfin/data   \
          -C /tmp/jellyfin          \
          -c /state/jellyfin/config \
          -l /state/jellyfin/log
      '';
    };

    ## Jackett
    services.jackett = {
      enable = true;
      dataDir = "/state/jackett";
    };

    ## Radarr
    services.radarr = {
      enable = true;
      dataDir = "/state/radarr";
    };
  };
}
