.PHONY: test
test: configuration.nix
	sudo nixos-rebuild $(ARGS) test --flake .
	rm result

.PHONY: switch
switch: configuration.nix
	sudo nixos-rebuild $(ARGS) switch --flake .

.PHONY: upgrade
upgrade:
	nix flake update --recreate-lock-file .
	git commit -m "make upgrade" flake.lock || true

.PHONY: gc
gc: upgrade switch
	nix-collect-garbage -d
	nix-store --optimize

.PHONY: push
push: switch
	git push

.PHONY: pull
pull:
	git pull -r

secret: secret.gpg
	gpg --pinentry-mode loopback -d $^ | tar xf -

.PHONY: secret.gpg
secret.gpg:
	tar cf - secret | gpg --pinentry-mode loopback -c > secret.gpg
