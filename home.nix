{ pkgs, secret, ... }:
let inherit (pkgs) lib;
in {

  ## Email
  programs = {
    mbsync.enable = true;
    msmtp.enable = true;
    mu.enable = true;
  };

  accounts.email.accounts = {
    work = {
      primary = true;
      passwordCommand = "cat ~/.mp";

      address = "khorsun_dv@dlit.dp.ua";
      userName = "khorsun_dv@dlit.dp.ua";
      realName = "khorsun_dv";

      imap.host = "outlook.com";
      smtp.host = "outlook.com";

      smtp.tls.useStartTls = true;

      mbsync = {
        enable = true;
        create = "maildir";
      };

      astroid.enable = true;
      msmtp.enable = true;
      notmuch.enable = true;
    };
  };

  home.file.".notmuch-config".text = ''
    [database]
    path=/home/u/Maildir

    [user]
    name=Khorsun Dmytro
    primary_email=khorsun_dv@dlit.dp.ua

    [new]
    tags=unread;inbox;
    ignore=

    [search]
    exclude_tags=deleted;spam;

    [maildir]
    synchronize_flags=true
  '';

  ## Files
  home.file = {
    ".ssh/keys" = {
      source = "${secret}/ssh";
      recursive = true;
    };
    ".editorconfig".source = toString ./dotfiles/editorconfig;
    ".local/bin" = {
      source = toString ./dotfiles/scripts;
      recursive = true;
    };
    ".config/gtk-3.0/bookmarks".text = ''
      file:///home/u/proj Projects
      file:///home/u/nix Configuration
    '';
    ".emacs.d/emacs.org".source = ./dotfiles/emacs.org;
    ".emacs".text = ''
      (setq custom-file (concat user-emacs-directory "custom.el"))
      (org-babel-load-file "~/.emacs.d/emacs.org")
    '';
  };

  ## Programs
  programs = {
    home-manager.enable = true;
    command-not-found.enable = true;
    bat.enable = true;

    ssh = {
      enable = true;
      serverAliveInterval = 120;
      matchBlocks = {
        gl = {
          hostname = "gitlab.com";
          user = "git";
          identitiesOnly = true;
          identityFile = "~/.ssh/keys/gl";
        };
        gh = {
          hostname = "github.com";
          user = "git";
          identitiesOnly = true;
          identityFile = "~/.ssh/keys/gh";
        };
      };
    };

    direnv = {
      enable = true;
      enableZshIntegration = true;
      enableBashIntegration = true;
      enableNixDirenvIntegration = true;
    };

    git = {
      enable = true;
      userName = "tsruser";
      userEmail = "tsruser@example.com";
      lfs.enable = true;
    };

    autojump.enable = true;

    bash = {
      enable = true;
      historyControl = [ "ignorespace" ];
      shellAliases = {
        r = "sudo nixos-rebuild";
        disk-size = "sudo du -hsx / --exclude '/home/*' | cut -f1";
        start-music = "nix shell nixpkgs#tmux nixpkgs#cmus -c tmux new cmus";
        show-music = "nix run nixpkgs#tmux a";
      };
      initExtra = ''
        export PATH="$PATH:$HOME/.local/bin";


        ix() {
            local opts
            local OPTIND
            [ -f "$HOME/.netrc" ] && opts='-n'
            while getopts ":hd:i:n:" x; do
                case $x in
                    h) echo "ix [-d ID] [-i ID] [-n N] [opts]"; return;;
                    d) $echo curl $opts -X DELETE ix.io/$OPTARG; return;;
                    i) opts="$opts -X PUT"; local id="$OPTARG";;
                    n) opts="$opts -F read:1=$OPTARG";;
                esac
            done
            shift $(($OPTIND - 1))
            [ -t 0 ] && {
                local filename="$1"
                shift
                [ "$filename" ] && {
                    curl $opts -F f:1=@"$filename" $* ix.io/$id
                    return
                }
                echo "^C to cancel, ^D to send."
            }
            curl $opts -F f:1='<-' $* ix.io/$id
        }
      '';
    };
  };

  ## Packages
  home.packages = with pkgs; [

    # Base
    gnumake
    gnutar
    kakoune
    man
    texinfo
    tree
    unzip
    zip
    (unstable.emacsWithPackagesFromUsePackage {
      config = ./dotfiles/emacs.org;
      alwaysEnsure = true;
    })

    # Useful tools
    ag
    aria
    fd
    htop
    nixfmt
  ];
}
