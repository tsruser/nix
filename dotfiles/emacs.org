#+title: My Emacs Config
#+autor: tsruser <tsruser@example.org>
#+options: toc:nil num:3 H:4 ^:nil pri:t html-style:nil
#+startup: content
#+html_doctype: html5
#+html_head: <link rel="stylesheet" href="https://gongzhitaao.org/orgcss/org.css"/>
#+bind: org-src-fontify-natively t
#+bind: org-html-htmlize-output-type 'css

#+begin_abstract
My single-file Emacs configuration.
#+end_abstract

#+toc: headlines 2

* Core Setup
** straight.el
#+BEGIN_SRC emacs-lisp
  (defvar bootstrap-version)
  (let ((bootstrap-file
         (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
        (bootstrap-version 5))
    (unless (file-exists-p bootstrap-file)
      (with-current-buffer
          (url-retrieve-synchronously
           "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
           'silent 'inhibit-cookies)
        (goto-char (point-max))
        (eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage))

  (setq straight-use-package-by-default t
        ;; https://github.com/raxod502/straight.el#april-19-2020
        straight-vc-git-default-clone-depth 1)

  (straight-use-package 'use-package)
#+END_SRC
** Auto-compile
#+BEGIN_SRC emacs-lisp
  (use-package auto-compile)
#+END_SRC
** Basic Stuff
*** Better Defaults
#+BEGIN_SRC emacs-lisp
  (use-package better-defaults)
#+END_SRC
*** Indent new line
#+BEGIN_SRC emacs-lisp
  (define-key global-map (kbd "RET") 'newline-and-indent)
#+END_SRC
*** Dired
#+begin_src emacs-lisp
  (with-eval-after-load "dired"
    (load "dired-x"))
#+end_src
*** Direnv
#+begin_src emacs-lisp
  (use-package direnv
    :config
    (direnv-mode t))
#+end_src
** Theme
#+BEGIN_SRC emacs-lisp
  (defun set-theme-based-on-time ()
    (interactive)
    (let* ((hours (caddr (decode-time)))
           (theme (if (and (> hours 7) (< hours 20))
                      'humanoid-light 'humanoid-dark)))
      (load-theme theme t)))

  (use-package humanoid-themes
    :straight (humanoid-themes :branch "main")
    :config
    (set-theme-based-on-time)
    (run-at-time "07:00" nil #'set-theme-based-on-time)
    (run-at-time "20:00" nil #'set-theme-based-on-time))
#+END_SRC
** Ivy
#+BEGIN_SRC emacs-lisp
  (use-package ivy
    :config
    (ivy-mode t))

  (use-package counsel
    :config
    (counsel-mode t))
#+END_SRC
** Org Mode
#+BEGIN_SRC emacs-lisp
  (eval-when-compile
    (setq-default org-src-fontify-natively t))

  (use-package htmlize)
#+END_SRC
* Programming
** General
*** Auto-Complete
#+BEGIN_SRC emacs-lisp
  (use-package auto-complete
    :config
    (ac-config-default))
#+END_SRC
*** Eglot
#+BEGIN_SRC emacs-lisp
  (use-package eglot
    :hook (prog-mode . eglot-ensure)
    :config
    (add-to-list 'eglot-server-programs
                 '((c-mode c++-mode) "clangd")))
#+END_SRC
*** SmartParens
#+BEGIN_SRC emacs-lisp
  (use-package smartparens
    :config
    (smartparens-global-mode t))
#+END_SRC
*** Git
#+BEGIN_SRC emacs-lisp
  (use-package magit
    :bind
    (("C-c C-g" . magit-status)
     ("C-c C-p" . magit-push)))
#+END_SRC
** Nix
#+begin_src emacs-lisp
  (use-package json-mode)

  (use-package nix-mode
    :mode "\\.nix\\'")

  (use-package company-nixos-options
    :after company
    :config
    (add-to-list 'company-backends 'company-nixos-options))
#+end_src
* Email
** Mu4e
*** Load mu4e
#+begin_src emacs-lisp
  (let ((mu4e-path
         (expand-file-name
          (concat
           (file-name-directory
            (executable-find "mu"))
           "../share/emacs/site-lisp/mu4e"))))
    (when (file-directory-p mu4e-path)
      (add-to-list 'load-path mu4e-path)
      (require 'mu4e)
      (setq mu4e-get-mail-command "mbsync -aCRX"
            sendmail-program (executable-find "msmtp"))))
#+end_src
*** Multiaccount hack from https://www.djcbsoftware.nl/code/mu/mu4e/Multiple-accounts.html
#+begin_src emacs-lisp
  (with-temp-buffer
    (insert-file-contents (concat user-emacs-directory "mu4e.sexp"))
    (let* ((accounts (read (buffer-string)))
           (default-account (cdar accounts)))
      (defvar mu4e-account-alist
        accounts)
      (mapc #'(lambda (var)
                (set (car var) (cadr var)))
            default-account)))

  (defun mu4e-set-account ()
    "Choose mu4e account."
    (let* ((account
            (if mu4e-compose-parent-message
                (let ((maildir (mu4e-message-field mu4e-compose-parent-message :maildir)))
                  (string-match "/\\(.*?\\)/" maildir)
                  (match-string 1 maildir))
              (completing-read (format "Compose with account: (%s) "
                                       (mapconcat #'car mu4e-account-alist "/"))
                               (mapcar #'car mu4e-account-alist)
                               nil t nil nil (caar mu4e-account-alist))))
           (account-vars (cdr (assoc account mu4e-account-alist))))
      (if account-vars
          (mapc #'(lambda (var) (set (car var) (cadr var)))
                account-vars)
        (error "Email account not found"))))

  (add-hook 'mu4e-compose-pre-hook #'mu4e-set-account)
#+end_src
