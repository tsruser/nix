{
  description = "My NixOS Config";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-20.09";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    home-manager = {
      url = "github:rycee/home-manager/master";
      inputs.nixpkgs.follows = "/nixpkgs";
    };

    emacs-overlay.url = "github:nix-community/emacs-overlay";

    secret = {
      type = "path";
      path = "/home/u/nix/secret";
      flake = false;
    };
  };

  outputs =
    { self, nixpkgs, nixpkgs-unstable, home-manager, emacs-overlay, secret }: {
      nixosConfigurations.u = nixpkgs.lib.nixosSystem rec {
        system = "x86_64-linux";
        specialArgs = { inherit secret; };
        modules = [
          home-manager.nixosModules.home-manager
          ({
            nixpkgs.overlays = [
              (final: prev: {
                unstable = import nixpkgs-unstable {
                  overlays = [ emacs-overlay.overlay ];
                  inherit system;
                };
              })
            ];
          })
          ({ pkgs, ... }: {
            nix = {
              extraOptions = ''
                experimental-features = nix-command flakes
                print-missing = true
              '';
              package = pkgs.nixFlakes;
              registry.nixpkgs.flake = nixpkgs;
            };
          })
          ./configuration.nix
          ./fix-spice-redirection.nix
        ];
      };

      devShell.x86_64-linux = let pkgs = nixpkgs.legacyPackages.x86_64-linux;
      in pkgs.mkShell { buildInputs = with pkgs; [ git gnupg gnumake ]; };
    };
}
