{ pkgs, ... }: {
  home-manager.config = import ./home.nix;
  user.shell = "${pkgs.bashInteractive}/bin/bash";
}
